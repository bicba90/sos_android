package com.sosService;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


public class WidgetService extends Service implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location loc;

    private static final long INTERVAL = 1000 * 20;
    private static final long FASTEST_INTERVAL = 1000 * 5;


    public WidgetService() {
        super();
    }


    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_SHORT).show();

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();


        final LocationManager manager = (LocationManager) getBaseContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(getBaseContext(), getBaseContext().getResources().getString(R.string.gps_off), Toast.LENGTH_SHORT).show();
            getBaseContext().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

        } else {
            createLocationRequest();
            mGoogleApiClient = new GoogleApiClient.Builder(WidgetService.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(WidgetService.this)
                    .addOnConnectionFailedListener(WidgetService.this)
                    .build();
            mGoogleApiClient.connect();
        }


        return START_STICKY;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        loc = location;

        //  String phoneNo = "+381652222796";
        String phoneNo =  SharedPreferencesUtil.getSettings(getBaseContext()).getPhone();
        String msg = getBaseContext().getResources().getString(R.string.sos_message);

        String myLocation = " http://www.google.com/maps/place/lat,lon/@lat,lon,14z";
        myLocation = myLocation.replaceAll("lat", String.valueOf(loc.getLatitude())).replaceAll("lon", String.valueOf(loc.getLongitude()));

        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phoneNo, null, msg + myLocation, null, null);
        Toast.makeText(this, getBaseContext().getResources().getString(R.string.message_sent), Toast.LENGTH_SHORT).show();
        stopLocationUpdates();

    }

    protected void startLocationUpdates() {
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        );
    }
}