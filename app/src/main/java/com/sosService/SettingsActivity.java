package com.sosService;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    EditText phoneNo;
    CheckBox vibration;
    Button save;
    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        phoneNo = (EditText) findViewById(R.id.number_et);
        vibration = (CheckBox) findViewById(R.id.settings_vibration);
        save = (Button) findViewById(R.id.settings_save);
        cancel = (Button) findViewById(R.id.settings_cancel);
        save.setOnClickListener(this);
        cancel.setOnClickListener(this);

        SettingsModel settingsModel = SharedPreferencesUtil.getSettings(this);

        vibration.setChecked(settingsModel.isVibration());
        phoneNo.setText(settingsModel.getPhone());
    }

    public void saveSettings() {
        SharedPreferencesUtil.saveSettings(SettingsActivity.this, new SettingsModel(phoneNo.getText().toString(), vibration.isChecked()));
    }

    @Override
    public void onClick(View v) {
        if (v == this.save) {
            if (ValidityChecker.validateRequired(SettingsActivity.this, phoneNo)) {
                saveSettings();
                finish();
            }
        } else if (v == this.cancel) {
            finish();
        }
    }
}
