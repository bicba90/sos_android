package com.sosService;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.widget.RemoteViews;
import android.widget.Toast;


public class CustomAppWidgetProvider extends AppWidgetProvider {

    private static final String BUTTON_SOS = "buttonSOS";
    RemoteViews remoteViews;
    Context context;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int count = appWidgetIds.length;
        this.context = context;
        for (int i = 0; i < count; i++) {
            int widgetId = appWidgetIds[i];

            remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.layout_widget);
            remoteViews.setTextViewText(R.id.textView, "");

            Intent intent = new Intent(context, CustomAppWidgetProvider.class);
            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            remoteViews.setOnClickPendingIntent(R.id.start_button,
                    getPendingSelfIntent(context, BUTTON_SOS));

            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (BUTTON_SOS.equals(intent.getAction())) {

            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                Toast.makeText(context, context.getResources().getString(R.string.gps_off), Toast.LENGTH_LONG).show();
            } else {
                context.stopService(new Intent(context, WidgetService.class));

                context.startService(new Intent(context, WidgetService.class));
            }


        }
    }

    ;

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }


}