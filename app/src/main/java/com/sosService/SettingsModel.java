package com.sosService;


import java.io.Serializable;

public class SettingsModel implements Serializable{

    private String phone;
    private boolean vibration;

    public SettingsModel(String phone, boolean vibration) {
        this.phone = phone;
        this.vibration = vibration;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isVibration() {
        return vibration;
    }

    public void setVibration(boolean vibration) {
        this.vibration = vibration;
    }
}
