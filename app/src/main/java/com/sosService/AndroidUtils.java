package com.sosService;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

public class AndroidUtils {

    private static final String LOG_TAG = "AndroidUtils";

    /**
     * Hiding soft keyboard
     *
     * @param activity
     */

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(new View(activity).getWindowToken(), 0);
    }

    /**
     * Showing soft keyboard
     *
     * @param activity
     * @param view
     */

    public static void showSoftKeyboard(Activity activity, View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    /**
     * Showing Toast message from string resources
     *
     * @param context
     * @param resourceId
     */

    public static void informUser(Context context, int resourceId) {

        Toast toast = Toast.makeText(context, context.getResources().getString(resourceId), Toast.LENGTH_LONG);
        View view = toast.getView();
        TextView text = (TextView) view.findViewById(android.R.id.message);
        toast.show();

    }

}
