package com.sosService;

import android.content.Context;
import android.preference.PreferenceManager;


public class SharedPreferencesUtil {

    private static final String SETTINGS_DATA = "settingsData";


    /**
     * Saves settings to shared preferences
     *
     * @param context
     * @param settings
     */
    public static void saveSettings(Context context, SettingsModel settings) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(SETTINGS_DATA, JSONUtils.toJson(settings)).apply();
    }

    /**
     * @param context
     * @return
     */
    public static SettingsModel getSettings(Context context) {
        try {
            return JSONUtils.fromJson(PreferenceManager.getDefaultSharedPreferences(context).getString(SETTINGS_DATA, ""), SettingsModel.class);
        } catch (Exception e) {
            return new SettingsModel("112", false);
        }
    }

    /**
     * @param context
     */
    public static void removeSettings(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove(SETTINGS_DATA).apply();
    }



}
