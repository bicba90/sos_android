package com.sosService;


import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import static android.content.ContentValues.TAG;


public class VolumeService extends Service implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

        private static final long INTERVAL = 1000 * 20;
        private static final long FASTEST_INTERVAL = 1000 * 5;

        LocationRequest mLocationRequest;
        GoogleApiClient mGoogleApiClient;
        Location loc;


        MediaPlayer mediaPlayer;

        public AudioManager myAudioManager;

        public VolumeService() {
            super();
        }


        int volumePrev = 0;
        boolean tacnaKombinacija[] = {true, false, false, false, true, false, false, false};
        int b = 0;
        long vreme;

        private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if ("android.media.VOLUME_CHANGED_ACTION".equals(intent.getAction())) {
                    int volume = intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", 0);

                    if (b == 0) {
                        if (volumePrev < volume) {
                            b++;
                            vreme = System.currentTimeMillis();
                        }
                    } else {
                        if (volumePrev < volume) {
                            if (tacnaKombinacija[b]) {
                                b++;
                            } else {
                                b = 0;
                                volumePrev = 0;
                            }
                        } else {
                            if (!tacnaKombinacija[b]) {
                                b++;
                            } else {
                                b = 0;
                                volumePrev = 0;
                            }
                        }
                    }
                    if (b == 7) {

                        if (System.currentTimeMillis() - vreme < 5000) {


                            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

                            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                                Toast.makeText(context, context.getResources().getString(R.string.gps_off), Toast.LENGTH_LONG).show();
                            else {
                                createLocationRequest();
                                mGoogleApiClient = new GoogleApiClient.Builder(VolumeService.this)
                                        .addApi(LocationServices.API)
                                        .addConnectionCallbacks(VolumeService.this)
                                        .addOnConnectionFailedListener(VolumeService.this)
                                        .build();
                                mGoogleApiClient.connect();
                            }

                        }
                        b = 0;
                        volumePrev = 0;
                    }
                    Log.i(TAG, "You have pressed " + b + " prosli " + volumePrev + " sadasnji" + volume);
                    volumePrev = volume;
                }
            }

        };


        @Override
        public void onDestroy() {
            mediaPlayer.stop();
            mediaPlayer.release();
            Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
            unregisterReceiver(broadcastReceiver);
            super.onDestroy();
        }

        @Nullable
        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

            mediaPlayer = MediaPlayer.create(this, R.raw.blank);
            mediaPlayer.setLooping(true);
            mediaPlayer.start();

            myAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

            IntentFilter filter = new IntentFilter();
            filter.addAction("android.media.VOLUME_CHANGED_ACTION");
            registerReceiver(broadcastReceiver, filter);

            return START_STICKY;
        }


        @Override
        public void onConnected(@Nullable Bundle bundle) {
            startLocationUpdates();
        }

        @Override
        public void onConnectionSuspended(int i) {

        }

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        }

        @Override
        public void onLocationChanged(Location location) {
            loc = location;

        //    String phoneNo = "+381652222796";
            String phoneNo =  SharedPreferencesUtil.getSettings(getBaseContext()).getPhone();
            String msg = getBaseContext().getResources().getString(R.string.sos_message);

            String myLocation = " http://www.google.com/maps/place/lat,lon/@lat,lon,14z";
            myLocation = myLocation.replaceAll("lat", String.valueOf(loc.getLatitude())).replaceAll("lon", String.valueOf(loc.getLongitude()));

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg + myLocation, null, null);
            Toast.makeText(this, getBaseContext().getResources().getString(R.string.message_sent), Toast.LENGTH_LONG).show();
            stopLocationUpdates();

        }

        protected void startLocationUpdates() {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }

        protected void createLocationRequest() {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(INTERVAL);
            mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        protected void stopLocationUpdates() {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient,
                    this
            );
        }
}