package com.sosService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class AppBootReceiver extends BroadcastReceiver {

    Context context;


    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        context.startService(new Intent(context, VolumeService.class));
    }



}