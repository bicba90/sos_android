package com.sosService;

import android.content.Context;
import android.widget.TextView;



public class ValidityChecker {



	public static boolean isNullOrEmpty(String value) {
		if (value == null) return true;
		if (value.trim().isEmpty()) return true;
		return false;
	}

	public static boolean validateRequired(Context c, TextView... editFields) {
		boolean result = true;
		for (TextView field : editFields) {
			if (field.getText().toString().isEmpty() || field.getText().toString().trim().isEmpty()) {
				field.setError(c.getResources().getText(R.string.required_field));
				result = false;
			} else {
				field.setError(null);
			}
		}
		return result;
	}


}
